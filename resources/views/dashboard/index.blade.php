@extends('layouts.dashboard')

@section('content')

<div class="section-header">
    <h1>Today Parking</h1>
</div>
<div class="section-body">
    <h2 class="section-title">Today Parking</h2>
    <p class="section-lead">Quick watch the parking activities today.</p>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>1st Floor</h4>
                </div>
                <div class="card-body">
                    <div class="gallery gallery-md">
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<img class="gallery-item" src="{{ asset('assets/img/car.png') }}">
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        	<h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">	
            <div class="card">
                <div class="card-header">
                    <h4>2nd Floor</h4>
                </div>
                <div class="card-body">
                    <div class="gallery gallery-md">
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                    </div>
                </div>
            </div>
        </div>
	
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>3rd Floor</h4>
                </div>
                <div class="card-body">
                    <div class="gallery gallery-md">
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/blank.svg') }}" data-title="Available">
                        <h1 style="width: 100px; height: 100px; top: 20%; left: 15%" class="gallery-item">B1</h4>
                        </div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                        <div class="gallery-item" data-image="{{ asset('assets/img/car.png') }}" data-title="Unavailable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection