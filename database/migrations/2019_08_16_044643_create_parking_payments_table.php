<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_payments', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('payment_method')->nullable();
            $table->string('account_number')->nullable();

            $table->double('amount_charged');
            $table->double('amount_paid')->default(0);

            $table->uuid('request_id');
            $table->foreign('request_id')
                ->references('id')
                ->on('parking_requests')
                ->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_payments');
    }
}
