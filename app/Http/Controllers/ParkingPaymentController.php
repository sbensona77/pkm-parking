<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Response;

use App\ParkingPayment;

class ParkingPaymentController extends Controller
{
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parkingpayments = ParkingPayment::all();

        return $parkingpayments;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            
        ]);

        try {
            $parkingpayment = new ParkingPayment($request->all());

            $parkingpayment->save();

            $message = 'Sukses membuat parkingpayment baru!';
        } catch (\Exception $e) {
            $message = 'Gagal membuat parkingpayment baru! : '.$e->getMessage();
        }

        return [
            'parkingpayments' => $parkingpayments,
            'message' => $message
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parkingpayment = ParkingPayment::findOrFail($id);
        
        return [
            'parkingpayment' => $parkingpayment
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parkingpayment = ParkingPayment::findOrFail($id);
        
        return [
            'parkingpayment' => $parkingpayment
        ]; 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([

        ]);

        try {
            $parkingpayment = ParkingPayment::findOrFail($id);
            
            $parkingpayment->fill($request->all());
            
            $parkingpayment->save();

            $message = 'Sukses memperbaharui parkingpayment data!';
        } catch (\Exception $e) {
            $message = 'Gagal memperbaharui parkingpayment data! : '.$e->getMessage();
        }

        return [
            'parkingpayment' => $parkingpayment,
            'message' => $message
        ];         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $parkingpayment = ParkingPayment::findOrFail($id);

            $parkingpayment->delete();

            $message = 'Sukse menghapus parkingpayment data!';
        } catch (Exception $e) {
            $message = 'Gagal menghapus parkingpayment data! : '.$e->getMessage();
        }

        return [
            'parkingpayment' => $parkingpayment,
            'message' => $message
        ];
    }
}