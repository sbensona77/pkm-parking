<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\UserProfile;

use App\ParkingSlot;
use App\ParkingRequest;
use App\ParkingPayment;

class APIController extends Controller
{
    public function registerUser(Request $request)
    {
    	$request->validate([
    		'name' => 'required',
			'email' => 'required',
			'password' => 'required',
			'id_card' => 'required',
			'police_number' => 'required',
    	]);

    	$user = new User();

    	$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->password = bcrypt($request->input('password'));
		$user->role = 'user';

		$user->save();

		$profile = new UserProfile();

		$profile->user_id = $user->id;
		$profile->id_card = $request->input('id_card');
		$profile->police_number = $request->input('police_number');

		$profile->save();

		return response()->json([
			'user' => $user,
			'profile' => $profile,
		]);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);
        $credentials['active'] = 1;
        $credentials['deleted_at'] = null;

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function requestParkingSlot(Request $request)
    {
    	$slots = ParkingSlot::where('is_taken', 0)->get();

    	$chosenSlot = $slots->first();

    	if($chosenSlot) {
    		$parkingRequest = new ParkingRequest();

    		$parkingRequest->user_id = $request->input('user_id') ? 
    			$request->input('user_id') : 'guest';
    		$parkingRequest->slot_id = $chosenSlot->id;

    		$parkingRequest->save();

    		$chosenSlot->is_taken = 1;
    		$chosenSlot->save();

    		$status = 'success';
    		$message = 'Parking ' . $chosenSlot->slot_code . ' is successfuly taken!';

    		$response = [
    			'status' => $status,
    			'message' => $message,
    			'parking_data' => [
    				'user' => $parkingRequest->user ? 
    					$parkingRequest->user : 'guess',
    				'request' => $parkingRequest,
    				'slot' => $chosenSlot,
    			],
    		];
    	} else {
    		$status = 'error';
    		$message = 'Parking is FULL!';

    		$response = [
    			'status' => $status,
    			'message' => $message,
    			'parking_data' => null,
    		];
    	}

    	return response()->json($response);
    }

    public function getAllParkingSlots(Request $request)
    {
    	$isTaken = $request->available_only;

    	$slots = ParkingSlot::all();

    	if($isTaken)
    		$slots = $slots->where('is_taken', 0);

    	return response()->json(['slots' => $slots]);
    }

    public function switchPosition(Request $request)
    {
    	$request->validate([
    		'parking_request_id' => 'required',
            'slot_id' => 'required',
    	]);

    	$parkingRequest = ParkingRequest::findOrFail($request->parking_request_id);

        if($slot = ParkingSlot::find($request->input('slot_id'))) {
    	   $parkingRequest->changeSlot($request->new_slot_id);

           $status = 'success';
           $message = 'Succeeded to take parking slot!';
        } else {
            $status = 'error';
            $message = 'Failed to take parking slot! Slot has been taken';
        }

    	return response()->json([
            'parking' => $parkingRequest
        ]);
    }

    public function generateParkingPayment(Request $request)
    {
    	$request->validate([
    		'parking_request_id' => 'required',
    		'price_per_hour' => 'required',
    	]);

    	$parkingRequest = ParkingRequest::findOrFail($request->input('parking_request_id'));
    	$parkingRequest->signOut();

    	$payment = new ParkingPayment();
		$payment->request_id = $parkingRequest->id;
		$payment->generateCharge();

		return response()->json([
			'parking_data' => $parkingRequest, 
			'payment' => $payment,
		]);
    }

    public function pay(Request $request)
    {
    	$request->validate([
    		'parking_payment_id' => 'required',
    		'payment_method' => 'required',
    		'amount_paid' => 'required',
    	]);

    	$payment = ParkingPayment::findOrFail($request->input('parking_payment_id'));
    	$payment->payment_method = $request->input('payment_method');
    	$payment->amount_paid = $request->input('amount_paid');
    	$payment->save();

    	return response()->json(['payment' => $payment]);
    }
}
