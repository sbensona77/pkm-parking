<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Response;

use App\ParkingSlot;

class ParkingSlotController extends Controller
{
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parkingslots = ParkingSlot::all();

        return $parkingslots;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            
        ]);

        try {
            $parkingslot = new ParkingSlot($request->all());

            $parkingslot->save();

            $message = 'Sukses membuat parkingslot baru!';
        } catch (\Exception $e) {
            $message = 'Gagal membuat parkingslot baru! : '.$e->getMessage();
        }

        return [
            'parkingslots' => $parkingslots,
            'message' => $message
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parkingslot = ParkingSlot::findOrFail($id);
        
        return [
            'parkingslot' => $parkingslot
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parkingslot = ParkingSlot::findOrFail($id);
        
        return [
            'parkingslot' => $parkingslot
        ]; 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([

        ]);

        try {
            $parkingslot = ParkingSlot::findOrFail($id);
            
            $parkingslot->fill($request->all());
            
            $parkingslot->save();

            $message = 'Sukses memperbaharui parkingslot data!';
        } catch (\Exception $e) {
            $message = 'Gagal memperbaharui parkingslot data! : '.$e->getMessage();
        }

        return [
            'parkingslot' => $parkingslot,
            'message' => $message
        ];         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $parkingslot = ParkingSlot::findOrFail($id);

            $parkingslot->delete();

            $message = 'Sukse menghapus parkingslot data!';
        } catch (Exception $e) {
            $message = 'Gagal menghapus parkingslot data! : '.$e->getMessage();
        }

        return [
            'parkingslot' => $parkingslot,
            'message' => $message
        ];
    }
}