<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Response;

use App\ParkingRequest;

class ParkingRequestController extends Controller
{
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parkingrequests = ParkingRequest::all();

        return $parkingrequests;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            
        ]);

        try {
            $parkingrequest = new ParkingRequest($request->all());

            $parkingrequest->save();

            $message = 'Sukses membuat parkingrequest baru!';
        } catch (\Exception $e) {
            $message = 'Gagal membuat parkingrequest baru! : '.$e->getMessage();
        }

        return [
            'parkingrequests' => $parkingrequests,
            'message' => $message
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parkingrequest = ParkingRequest::findOrFail($id);
        
        return [
            'parkingrequest' => $parkingrequest
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parkingrequest = ParkingRequest::findOrFail($id);
        
        return [
            'parkingrequest' => $parkingrequest
        ]; 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([

        ]);

        try {
            $parkingrequest = ParkingRequest::findOrFail($id);
            
            $parkingrequest->fill($request->all());
            
            $parkingrequest->save();

            $message = 'Sukses memperbaharui parkingrequest data!';
        } catch (\Exception $e) {
            $message = 'Gagal memperbaharui parkingrequest data! : '.$e->getMessage();
        }

        return [
            'parkingrequest' => $parkingrequest,
            'message' => $message
        ];         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $parkingrequest = ParkingRequest::findOrFail($id);

            $parkingrequest->delete();

            $message = 'Sukse menghapus parkingrequest data!';
        } catch (Exception $e) {
            $message = 'Gagal menghapus parkingrequest data! : '.$e->getMessage();
        }

        return [
            'parkingrequest' => $parkingrequest,
            'message' => $message
        ];
    }
}