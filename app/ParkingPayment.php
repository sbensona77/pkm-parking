<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

use Carbon\Carbon;

class ParkingPayment extends Model
{
    protected $table = 'parking_payments';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'payment_method',
        'account_number',

        'amount_charged',
        'amount_paid',

        'request_id',
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});
    }

    public static function generateCharge($pricePerHour, $signInTime, $signOutTime)
    {
        $start = Carbon::parse($signInTime);
        $end = Carbon::parse($signOutTime);

        $durationMinute = $end->diffInMinutes($start);
        $durationHour = ceil($end->diffInMinutes($start) / 60);

        $charged = $pricePerHour * $durationHour;

        $this->attribute['amount_charged'] = $charged;

        return $this->save();
    }

    public function request()
    {
        return $this->belongsTo('App\ParkingRequest', 'request_id', 'id');
    }
}