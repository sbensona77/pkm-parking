<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Webpatser\Uuid\Uuid;

use Carbon\Carbon;

class ParkingRequest extends Model
{
    protected $table = 'parking_requests';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = false;

    protected $fillable = [
        'user_id',
        'slot_id',
    ];

    protected $hidden = [
        
    ];

    protected static function boot()
    {
    	parent::boot();

    	self::creating(function($model) {
            $model->id = Uuid::generate()->string;
    	});
    }

    public static function signOut()
    {
        $this->attribute['sign_out'] = Carbon::now();

        return $this->save();
    }

    public static function changeSlot($newSlotId)
    {
        $this->attribute['slot_id'] = $newSlotId;

        return $this->save();
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function slot()
    {
        return $this->hasOne('App\ParkingSlot', 'slot_id', 'id');
    }
}