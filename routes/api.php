<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/slots', 'APIController@getAllParkingSlots');
Route::get('/request_parking', 'APIController@requestParkingSlot');
Route::get('/generate_payment', 'APIController@generateParkingPayment');
Route::get('/logout', 'APIController@logout');

Route::post('/register', 'APIController@registerUser');
Route::post('/pay', 'APIController@pay');
Route::post('/login', 'APIController@login');
